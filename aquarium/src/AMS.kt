import java.util.*

fun main(args: Array<String>) {
    println("Hello, world!")
    feedTheFish()
    canAddFish(10.0, listOf(3,3,3))
    canAddFish(8.0, listOf(2,2,2), hasDecorations = false)
    canAddFish(9.0, listOf(1,1,3), 3)
    canAddFish(10.0, listOf(), 7, true)
}



fun canAddFish(tankSize: Double, currentFish: List<Int>, fishSize: Int = 2, hasDecorations: Boolean = true): Boolean {
    var realisticTankSize = tankSize;
    if(hasDecorations) realisticTankSize *= 0.8

    val availableSpace =  realisticTankSize - currentFish.sum() - fishSize;
    println(availableSpace >= 0)
    return availableSpace >= 0
}

fun feedTheFish() {
    val day = randomDay();
    val food = fishFood(day)
    println("Today is $day and the fish eat $food")
    if(shouldChangeWater(day))
        println("Should change the water")
}

fun shouldChangeWater(
    day: String,
    temperature: Int = 22,
    dirty: Int = 20
): Boolean {
    return when {
       isTooHot(temperature) -> true
        isDirty(dirty) -> true
        isSunday(day) -> true
        else -> false
    }
}

fun isTooHot(temperature: Int) = temperature > 30
fun isDirty (dirty: Int) = dirty > 30
fun isSunday (day: String) = day == "sunday"


fun randomDay(): String {
    val week = listOf<String>("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
    return week[Random().nextInt(week.size)]
}

fun fishFood(day: String) : String {
    return when (day) {
        "Monday" -> "flakes"
        "Tuesday" -> "pellets"
        "Wednesday" -> "granules"
        "Friday" -> "mosquitoes"
        "Sunday" -> "plankton"
        else -> "fasting"
    }
}
