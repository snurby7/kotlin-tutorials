package Aquarium

fun main() {
    delegate()
}

fun delegate() {
    val pleco = PlecostomusTut()
    println("Fish has color ${pleco.color}")
    pleco.eat();
}

interface FishActionTut {
    fun eat()
}

interface FishColor {
    val color: String
}

class PlecostomusTut : FishAction by PrintingFishAction("a lot of algae"), FishColor by GoldColor {
}

object GoldColor : FishColor {
    override val color: String
        get() = "gold"
}

class PrintingFishAction(val food: String) : FishAction {
    override fun eat() {
        println(food)
    }
}