package Book

import java.util.*

const val MAX_NUMBER_OF_BOOKS = 20

open class Book(var title: String, var author: String, val year: Int, var pages: Int = 50) {
    companion object {
        val BASE_URL = "http://turtlecare.net/"
    }

    private var currentPage = 0
    open fun readPage() {
        currentPage++
    }

    fun getTitleAuthor(): Pair<String, String> {
        return (title to author)
    }

    fun getTitleAuthorYear(): Triple<String, String, Int> {
        return Triple(title, author, year)
    }

    fun canBorrow(hasBooks: Int): Boolean {
        return hasBooks < MAX_NUMBER_OF_BOOKS
    }

    fun printUrl(): String {
        return "$BASE_URL/$title.html"
    }
}

fun Book.weight() : Double { return (pages * 1.5) }
fun Book.tornPages(torn: Int) = if (pages >= torn) pages -= torn else pages = 0

class eBook(var format: String = "text", title: String, author: String, year: Int) : Book(title, author, year) {
    private var wordsRead = 0
    override fun readPage() {
        wordsRead += 250
    }
}

class Puppy() {
    fun playWithBook(book: Book) {
        book.tornPages(Random().nextInt(12))
    }
}

fun main(args: Array<String>) {

    val book = Book("Romeon and Juliet", "William Shakespeare", 1597)
    val bookTitleAuthor = book.getTitleAuthor()
    val bookTitleAuthorYear = book.getTitleAuthorYear()

    println("Here is your book ${bookTitleAuthor.first} by ${bookTitleAuthor.second}")

    println(
        "Here is your book ${bookTitleAuthorYear.first} " +
                "by ${bookTitleAuthorYear.second} written in ${bookTitleAuthorYear.third}"
    )

    val allBooks = setOf("Macbeth", "Romeo and Juliet", "Hamlet", "A Midsummer Night's Dream")
    val library = mapOf("Shakespeare" to allBooks)
    println(library.any { it.value.contains("Hamlet") })

    val moreBooks = mutableMapOf<String, String>("Wilhelm Tell" to "Schiller")
    moreBooks.getOrPut("Jungle Book") { "Kipling" }
    moreBooks.getOrPut("Hamlet") { "Shakespeare" }
    println(moreBooks)

    val puppy = Puppy()
    val dogBook = Book("Oliver Twist", "Charles Dickens", 1837, 540)

    while (dogBook.pages > 0) {
        puppy.playWithBook(dogBook)
        println("${dogBook.pages} left in ${dogBook.title}")
    }
    println("Sad puppy, no more pages in ${dogBook.title}. ")

}